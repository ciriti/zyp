package com.itech.samplelocation.presentation.feature.scooterlocation

import arrow.core.Either

interface UseCase {
    suspend fun getLocation4View(): Either<Throwable, List<Location4View>>
    suspend fun getInfo4View(id: Int): Either<Throwable, Info4View>
    companion object
}