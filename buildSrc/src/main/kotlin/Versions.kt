import kotlin.String

/**
 * Find which updates are available by running
 *     `$ ./gradlew syncLibs`
 * This will only update the comments.
 *
 * YOU are responsible for updating manually the dependency version. */
object Versions {
    const val core_testing: String = "1.1.1" 

    const val appcompat: String = "1.1.0-rc01" // exceed the version found: 1.0.2

    const val cardview: String = "1.0.0" 

    const val constraintlayout: String = "2.0.0-beta2" // exceed the version found: 1.1.3

    const val core_ktx: String = "1.0.2" 

    const val espresso_core: String = "3.2.0" 

    const val androidx_test_ext_junit: String = "1.1.0" //available: "1.1.1" 

    const val uiautomator: String = "2.2.0" 

    const val androidx_test: String = "1.2.0" 

    const val com_android_tools_build_gradle: String = "3.4.2" 

    const val lint_gradle: String = "26.4.2" 

    const val play_services_maps: String = "17.0.0" 

    const val material: String = "1.0.0" 

    const val retrofit2_kotlin_coroutines_adapter: String = "0.9.2" 

    const val com_squareup_okhttp3: String = "4.0.0" //available: "4.0.1" 

    const val com_squareup_retrofit2: String = "2.5.0" //available: "2.6.0" 

    const val arrow_core: String =
            "0.8.1" // No update information. Is this dependency available on jcenter or mavenCentral?

    const val io_mockk: String = "1.9.2" //available: "1.9.3" 

    const val jmfayard_github_io_gradle_kotlin_dsl_libs_gradle_plugin: String = "0.2.6" 

    const val junit_junit: String = "4.12" 

    const val hamcrest_library: String = "2.1" 

    const val org_jetbrains_kotlin: String = "1.3.41" 

    const val kotlinx_coroutines_android: String = "1.2.1" //available: "1.2.2" 

    const val org_koin: String = "2.0.1" 

    const val android_arch_lifecycle_extensions: String = "1.1.1"

    /**
     *
     *   To update Gradle, edit the wrapper file at path:
     *      ./gradle/wrapper/gradle-wrapper.properties
     */
    object Gradle {
        const val runningVersion: String = "5.1.1"

        const val currentVersion: String = "5.5.1"

        const val nightlyVersion: String = "5.7-20190722220035+0000"

        const val releaseCandidate: String = ""
    }
}
