package com.itech.datalayer

import com.google.gson.annotations.SerializedName

data class Location (
    @SerializedName("id")
    val id:  Int ,
    @SerializedName("latitude")
    val latitude : Double ,
    @SerializedName("longitude")
    val longitude : Double,
    @SerializedName("name")
    val name: String ,
    @SerializedName("description")
    val description : String ,
    @SerializedName("batteryLevel")
    val batteryLevel : Int ,
    @SerializedName("timestamp")
    val timestamp : String ,
    @SerializedName("price")
    val price : Int,
    @SerializedName("priceTime")
    val priceTime : Int ,
    @SerializedName("currency")
    val currency : String
)