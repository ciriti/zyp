package com.itech.samplelocation.presentation.feature.scooterlocation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import arrow.core.Either
import com.itech.datalayer.util.Location
import com.itech.samplelocation.presentation.feature.scooterlocation.BaseState.StateError
import com.itech.samplelocation.presentation.feature.scooterlocation.BaseState.StatePositions
import com.util.test.TestUtilGson
import com.util.test.assertEquals
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

class ScooterLocationViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @MockK lateinit var useCase: UseCase

    val mockList by lazy {
        TestUtilGson.run { "vehicles.json".createListObjByJsonFile<List<Location>>() }
        .map { it.toLocation4View() }
    }

    val mockInfo by lazy {
        TestUtilGson.run { "vehicle_1.json".createGsonObj<Location>() }
            .let { it.toInfo4View() }
    }

    val viewModel by lazy {
        ScooterLocationViewModel(
            dispatcher = Dispatchers.Unconfined,
            useCase = useCase
        )
    }

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun `fetch locations success case`(){
        coEvery { useCase.getLocation4View() } returns Either.right(mockList)

        viewModel.fetchPositions()

        val res = viewModel.liveData.value as StatePositions

        res.data assertEquals mockList
    }

    @Test
    fun `fetch info success case`(){
        coEvery { useCase.getInfo4View(any()) } returns Either.right(mockInfo)

        viewModel.fetchInfo(1)

        val res = viewModel.liveData.value as BaseState.StateInfo

        res.info assertEquals mockInfo
    }

    @Test
    fun `fetch locations no network case`(){
        coEvery { useCase.getLocation4View() } returns Either.Left(UnknownHostException())

        viewModel.fetchPositions()

        val res = viewModel.liveData.value as StateError

        res.errorMessage assertEquals  "No internet available"
    }

    @Test
    fun `fetch info error case with RuntimeException`(){
        coEvery { useCase.getInfo4View(any()) } returns Either.Left(RuntimeException())

        viewModel.fetchInfo(1)

        val res = viewModel.liveData.value as StateError

        res.errorMessage assertEquals  "Something went wrong"
    }

}