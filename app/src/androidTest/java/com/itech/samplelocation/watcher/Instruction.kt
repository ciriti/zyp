package com.itech.samplelocation.watcher

abstract class Instruction {

    var watcherTimeout = ConditionWatcher.DEFAULT_TIMEOUT_LIMIT
    var watchInterval = ConditionWatcher.DEFAULT_INTERVAL

    abstract fun checkCondition(): Boolean

    open fun onTimeout(instruction: Instruction) {
        throw RuntimeException("We have been waiting for ${instruction.watcherTimeout / 1000} seconds but the condition has not been met")
    }
}