package com.itech.samplelocation.presentation.feature.scooterlocation

import arrow.core.Either
import arrow.core.Try
import com.itech.datalayer.datasource.Datasource

// factory method
fun UseCase.Companion.build(ds: Datasource): UseCase = UseCaseImpl(ds)

private class UseCaseImpl(val ds: Datasource) : UseCase {

    override suspend fun getLocation4View(): Either<Throwable, List<Location4View>> {

        val list : Try<List<Location4View>> = ds.getLocations()
            .flatMap { locations ->  Try{ locations.map { loc ->  loc.toLocation4View() } } }

        return list.toEither()
    }

    override suspend fun getInfo4View(id: Int): Either<Throwable, Info4View> {
        return ds.getInfo(id)
            .flatMap { Try { it.toInfo4View() } } // catch the exception
            .toEither()
    }
}