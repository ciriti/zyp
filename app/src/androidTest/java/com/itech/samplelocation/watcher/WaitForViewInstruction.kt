package com.itech.samplelocation.watcher

class WaitForViewInstruction constructor(
    timeout: Int = ConditionWatcher.DEFAULT_TIMEOUT_LIMIT,
    private val task: () -> Unit
) : Instruction() {

    private lateinit var lastException: Throwable

    init {
        watcherTimeout = timeout
    }

    override fun checkCondition(): Boolean {
        return try {
            task()
            true
        } catch (th: Throwable) {
            lastException = th
            false
        }
    }

    override fun onTimeout(instruction: Instruction) {
        throw Exception("Waited for ${watcherTimeout / 1000} seconds", lastException)
    }
}