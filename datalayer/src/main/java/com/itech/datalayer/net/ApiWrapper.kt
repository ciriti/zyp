package com.itech.datalayer.net

import arrow.core.Try
import com.itech.datalayer.util.Location

interface ApiWrapper {
    suspend fun getLocations(): Try<List<Location>>
    suspend fun getInfo(id: Int) : Try<Location>
    companion object
}