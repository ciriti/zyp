package com.itech.samplelocation.presentation.feature.scooterlocation

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.itech.datalayer.util.GenericException
import com.itech.datalayer.util.Location
import java.net.UnknownHostException

interface ViewBean

data class Location4View(
    val id:  Int ,
    val latitude : Double ,
    val longitude : Double
) : ViewBean

data class Info4View(
    val id:  Int ,
    val name: String ,
    val description : String ,
    val batteryLevel : Int ,
    val timestamp : String ,
    val price : Int,
    val priceTime : Int ,
    val currency : String
) : ViewBean{
    override fun toString(): String {
        return """
            $description - $name
            battery: $batteryLevel%
            price: $price $currency
            priceTime : $priceTime
        """.trimIndent()
    }
}

fun Location.toLocation4View() : Location4View =
    Location4View(
        id = id,
        latitude = latitude,
        longitude = longitude
    )

fun Location.toInfo4View() : Info4View =
    Info4View(
        id = id,
        name = name,
        batteryLevel = batteryLevel,
        currency = currency,
        description = description,
        price = price,
        priceTime = priceTime,
        timestamp = timestamp
    )

fun Location4View.toMarkerOptions() = MarkerOptions().position(LatLng(this.latitude, this.longitude))
fun Location4View.toLatLng() = LatLng(this.latitude, this.longitude)

sealed class BaseState{
    data class StatePositions(val data: List<Location4View>) : BaseState()
    data class StateInfo(val info: Info4View) : BaseState()
    data class StateError(val errorMessage: String) : BaseState()
}


fun getErrorMessage(throwable: Throwable) : String = when(throwable){
    is UnknownHostException -> "No internet available"
    is GenericException -> throwable.message?:"error"
    else -> "Something went wrong"
}
