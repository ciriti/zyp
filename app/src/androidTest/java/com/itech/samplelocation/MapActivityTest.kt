package com.itech.samplelocation

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import com.itech.datalayer.util.Location
import com.itech.samplelocation.presentation.MapsActivity
import com.itech.samplelocation.presentation.feature.scooterlocation.BaseState
import com.itech.samplelocation.presentation.feature.scooterlocation.BaseState.StatePositions
import com.itech.samplelocation.presentation.feature.scooterlocation.ScooterLocationViewModel
import com.itech.samplelocation.presentation.feature.scooterlocation.toInfo4View
import com.itech.samplelocation.presentation.feature.scooterlocation.toLocation4View
import com.util.test.TestUtilGson
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@RunWith(AndroidJUnit4::class)
class MapActivityTest : KoinTest {

    @get:Rule
    val activityRule = ActivityTestRule(MapsActivity::class.java, false, false)

    @MockK
    lateinit var mockViewModel: ScooterLocationViewModel
    @MockK
    lateinit var vmFactoryMock: ViewModelProvider.Factory

    val liveData = MutableLiveData<BaseState>()

    val mockModule = module(override = true){
        single { vmFactoryMock }
        viewModel{ mockViewModel }
    }

    val mockList by lazy {
        TestUtilGson.run { "vehicles.json".createListObjByJsonFile<List<Location>>() }
            .map { it.toLocation4View() }
    }

    val mockInfo by lazy {
        TestUtilGson.run { "vehicle_1.json".createGsonObj<Location>().toInfo4View() }
    }

    @Before
    fun setup() {
        /** mocked variable */
        MockKAnnotations.init(this, relaxUnitFun = true)
        every { vmFactoryMock.create(ScooterLocationViewModel::class.java) } returns mockViewModel
        every { mockViewModel.liveData } returns liveData
        every { mockViewModel.fetchInfo(any()) } answers { liveData.value = BaseState.StateInfo(mockInfo)  }
        activityRule.runOnUiThread { liveData.value = StatePositions(mockList) }
    }

    @Test
    fun mapActivity_should_display_the_scooter_info_after_a_chick_on_marker() {
        // 1. replace the module
        loadKoinModules(mockModule)
        // 2. start activity
        activityRule.launchActivity(Intent())

        MarkerSelectionAssistant()
            .isViewNotVisibleOnMap(R.id.info_text)
            .selectScooterMarker()
            .isViewVisibleOnMap(R.id.info_text)
            .clickOnTheMap()
    }
}
