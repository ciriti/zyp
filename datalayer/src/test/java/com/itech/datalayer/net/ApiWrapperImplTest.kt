package com.itech.datalayer.net

import arrow.core.Try
import com.itech.datalayer.util.Location
import com.itech.datalayer.util.Logger
import com.util.test.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.async
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class ApiWrapperImplTest {

    @MockK lateinit var api : Api
    @MockK lateinit var logger: Logger
    @MockK lateinit var responseBody: ResponseBody

    val apiImpl by lazy { ApiWrapper.build(api, logger) }

    /**
     * files are in root/sharedtestutil/jsonFiles
     */
    val mockList by lazy { TestUtilGson.run { "vehicles.json".createListObjByJsonFile<List<Location>>() } }
    val mockInfo by lazy { TestUtilGson.run { "vehicle_1.json".createGsonObj<Location>() } }

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun ` get Locations success case `() = runBlockingUnit{

        every { api.getLocations() } returns async { Response.success(mockList) }

        val res : Try<List<Location>> = apiImpl.getLocations()
        (res as Try.Success).assertNotNull()
        res.value.size assertEquals mockList.size
    }

    @Test
    fun ` get Locations error case `() = runBlockingUnit{
        every { api.getLocations() } returns async { Response.error<List<Location>>(400, responseBody) }

        val res : Try<List<Location>> = apiImpl.getLocations()
        (res as Try.Failure).assertNotNull()
    }


    @Test
    fun ` retrieve info success case `() = runBlockingUnit{

        every { api.getInfo(1) } returns async { Response.success(mockInfo) }

        val res : Response<Location> = api.getInfo(1).await()
        res.isSuccessful.assertTrue()
        res.body() assertEquals mockInfo
    }

    @Test
    fun ` retrieve info error case `() = runBlockingUnit{
        coEvery { api.getInfo(1) } returns async { Response.error<Location>(400, responseBody) }

        val res : Response<Location> = api.getInfo(1).await()
        res.isSuccessful.assertFalse()
        res.errorBody().assertNotNull()
    }

}