package com.itech.samplelocation.presentation.feature.scooterlocation

import arrow.core.Either
import arrow.core.Try
import com.itech.datalayer.util.GenericException
import com.itech.datalayer.util.Location
import com.itech.datalayer.datasource.Datasource
import com.util.test.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class UseCaseImplTest {

    @MockK lateinit var ds : Datasource
    val usecase by lazy { UseCase.build(ds) }

    /**
     * files are in root/sharedtestutil/jsonFiles
     */
    val mockList by lazy { TestUtilGson.run { "vehicles.json".createListObjByJsonFile<List<Location>>() } }
    val mockInfo by lazy { TestUtilGson.run { "vehicle_1.json".createGsonObj<Location>() } }

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun ` get Locations success case, it will return a map MarkerOptions-Location `() = runBlockingUnit{

        coEvery { ds.getLocations() } returns Try { mockList }

        val res : Either<Throwable, List<Location4View>> = usecase.getLocation4View()
        (res as Either.Right<List<Location4View>>).assertNotNull()
        res.b.size assertEquals mockList.size
    }

    @Test
    fun ` get Locations error case, it has to return a Left object `() = runBlockingUnit{

        coEvery { ds.getLocations() } returns Try { throw GenericException() }
        val res : Either<Throwable, List<Location4View>> = usecase.getLocation4View ()
        (res as Either.Left).assertNotNull()
        (res.a as GenericException).assertNotNull()

    }


    @Test
    fun ` get info success case, it will return the scooter info `() = runBlockingUnit{

        coEvery { ds.getInfo(any()) } returns Try { mockInfo }
        val res : Either<Throwable, Info4View> = usecase.getInfo4View(1)
        (res as Either.Right<Info4View>).assertNotNull()
        res.b.id assertEquals mockInfo.id

    }


    @Test
    fun ` get info error case, it has to return an Info4View Left object `() = runBlockingUnit{

        coEvery { ds.getInfo(any()) } returns Try { throw GenericException() }
        val res : Either<Throwable, Info4View> = usecase.getInfo4View(1)
        (res as Either.Left).assertNotNull()
        (res.a as GenericException).assertNotNull()

    }

}