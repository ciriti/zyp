package com.itech.samplelocation

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.itech.samplelocation.watcher.ConditionWatcher
import com.itech.samplelocation.watcher.WaitForViewInstruction
import org.hamcrest.Matchers.not

object ViewVerificationHelper {
    @Throws(Exception::class)
    fun waitAndPerform(timeout: Int = ConditionWatcher.DEFAULT_TIMEOUT_LIMIT, task: () -> Unit) {
        ConditionWatcher.waitFor(WaitForViewInstruction(timeout, task))
    }

    fun viewIsVisible(@IdRes id: Int) {
        onView(withId(id)).check(matches(isDisplayed()))
    }

    fun viewIsNotVisible(@IdRes id: Int) {
        onView(withId(id)).check(matches(not(isDisplayed())))
    }

    fun viewWithStringIdIsVisible(@StringRes id: Int) {
        onView(withText(id)).check(matches(isDisplayed()))
    }

    fun viewWithTextIsVisible(text: String) {
        onView(withText(text)).check(matches(isDisplayed()))
    }
}