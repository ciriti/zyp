package com.itech.datalayer.util

interface Logger {
    fun info(tag : String, message : String)
    fun debug(tag : String, message : String)
    fun error(tag : String, message : String)
    fun verbose(tag : String, message : String)
    companion object
}