package com.itech.samplelocation.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class ViewModelFactory (
    val args : Map<Class<out ViewModel>, ViewModel>
) : ViewModelProvider.Factory {

  override fun <T : ViewModel> create(modelClass: Class<T>): T {

    val creator : ViewModel? = args[modelClass]

    try {
      return creator as? T ?: throw IllegalArgumentException("Unknown ViewModel class")
    } catch (e: Exception) {
      throw RuntimeException(e)
    }
  }
}