package com.itech.samplelocation

import com.itech.samplelocation.ViewVerificationHelper.viewIsNotVisible
import com.itech.samplelocation.ViewVerificationHelper.viewIsVisible
import com.itech.samplelocation.ViewVerificationHelper.waitAndPerform

open class BaseAssistant {

    fun isViewVisible(id: Int) {
        waitAndPerform {
            viewIsVisible(id)
        }
    }

    fun isViewNotVisible(id: Int) {
        waitAndPerform {
            viewIsNotVisible(id)
        }
    }

}