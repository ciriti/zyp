package com.itech.samplelocation.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.itech.datalayer.BuildConfig
import com.itech.datalayer.util.Logger
import com.itech.datalayer.util.build
import com.itech.datalayer.util.createCoroutinesAdapter
import com.itech.datalayer.datasource.Datasource
import com.itech.datalayer.datasource.build
import com.itech.datalayer.net.Api
import com.itech.datalayer.net.ApiWrapper
import com.itech.datalayer.net.build
import com.itech.samplelocation.core.ViewModelFactory
import com.itech.samplelocation.presentation.feature.scooterlocation.ScooterLocationViewModel
import com.itech.samplelocation.presentation.feature.scooterlocation.UseCase
import com.itech.samplelocation.presentation.feature.scooterlocation.build
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val appModule = module(override = true) {

    // viewmodels map
    single {
        val vmDetailsMovie: ViewModel = get<ScooterLocationViewModel>()
        val map: Map<Class<out ViewModel>, ViewModel> = mapOf(
            ScooterLocationViewModel::class.java to vmDetailsMovie
        )
        map
    }

    // factory
    single<ViewModelProvider.Factory> { ViewModelFactory(get()) }

    // ViewModel
    viewModel(override = true) { ScooterLocationViewModel(useCase = get()) }

    // UseCase
    factory { UseCase.build(ds = get())  }

    // Datasource
    single { Datasource.build(apiWrapper = get(), logger = get()) }

    // Api wrapper
    single { ApiWrapper.build(api = get(), logger = get()) }

    // Retrofit adapter
    single { Retrofit.Builder().createCoroutinesAdapter<Api>(BuildConfig.URL) }

    // Logger
    single { Logger.build() }

}