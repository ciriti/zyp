package com.itech.datalayer.net

import com.itech.datalayer.BuildConfig
import com.itech.datalayer.util.Location
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET(BuildConfig.URL + "FlashScooters/Challenge/vehicles/")
    fun getLocations() : Deferred<Response<List<Location>>>

    @GET(BuildConfig.URL + "FlashScooters/Challenge/vehicles/{id}")
    fun getInfo(@Path("id") id : Int) : Deferred<Response<Location>>

}