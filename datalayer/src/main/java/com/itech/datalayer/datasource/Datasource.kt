package com.itech.datalayer.datasource

import arrow.core.Try
import com.itech.datalayer.util.Location

interface Datasource {
    suspend fun getLocations(): Try<List<Location>>
    suspend fun getInfo(id: Int) : Try<Location>
    companion object
}