package com.itech.samplelocation.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.itech.samplelocation.R
import com.itech.samplelocation.presentation.feature.scooterlocation.*
import kotlinx.android.synthetic.main.layout_map.*
import org.koin.android.ext.android.inject

class MapsActivity : BaseMapsActivity() {

    private lateinit var mMap: GoogleMap
    private var map : Map<Marker, Location4View> = emptyMap()

    val viewModelFactory: ViewModelProvider.Factory by inject()
    lateinit var viewModel : ScooterLocationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.layout_map)
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ScooterLocationViewModel::class.java]
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapClickListener {
            container_motion.transitionToState(R.id.collapsed)
        }
        mMap.uiSettings.run {
            isMapToolbarEnabled = false
        }
        viewModel.liveData.observe(this, Observer { resultHandler(it) })
        viewModel.fetchPositions()
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        map[marker]?.let { viewModel.fetchInfo(it.id) }
        return false
    }

    private fun resultHandler(state: BaseState) = when (state) {
        is BaseState.StateError -> { Toast.makeText(this, state.errorMessage, Toast.LENGTH_SHORT).show() }
        is BaseState.StatePositions -> {
            map = state.data.map { Pair(mMap.addMarker(it.toMarkerOptions()), it) }.toMap()
            state.data.firstOrNull()?.let {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(it.toLatLng(), 18.0F))
            }
        }
        is BaseState.StateInfo -> {
            info_text.text = state.info.toString()
            container_motion.transitionToState(R.id.expanded)
        }
    }
}
