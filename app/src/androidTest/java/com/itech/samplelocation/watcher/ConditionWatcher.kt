package com.itech.samplelocation.watcher

class ConditionWatcher private constructor() {

    @Throws(Exception::class)
    private fun waitForCondition(instruction: Instruction) {
        var elapsedTime = 0L

        do {
            if (instruction.checkCondition())
                return
            else {
                elapsedTime += instruction.watchInterval
                Thread.sleep(instruction.watchInterval)
            }
        } while (elapsedTime < instruction.watcherTimeout)

        instruction.onTimeout(instruction)
    }

    companion object {

        fun waitFor(instruction: Instruction) {
            return ConditionWatcher().waitForCondition(instruction)
        }

        const val DEFAULT_TIMEOUT_LIMIT = 1000 * 5
        const val DEFAULT_INTERVAL = 250L
    }

}