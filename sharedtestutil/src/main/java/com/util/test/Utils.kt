package com.util.test

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

interface TestUtilGson{

    companion object{
        /**
         * Parse file.json and return list of objects
         */
        inline fun <reified K> String.createListObjByJsonFile(): K {
            val typeToken = object : TypeToken<K>() {}.type
            val jsonString = this.jsonFile2String()
            return Gson().fromJson(jsonString, typeToken)
        }

        /**
         * Receive file.json and return the content as string
         */
        fun String.jsonFile2String(): String = Thread.currentThread()
            .contextClassLoader
            .getResourceAsStream(this)
            .bufferedReader().use { it.readText() }

        /**
         * Receive a file.json and create a reified Object
         */
        inline fun <reified T> String.createGsonObj(): T {
            val jsonString = this.jsonFile2String()
            return Gson().fromJson(jsonString, T::class.java)
        }
    }
}

infix fun <T> T.assertEquals(t: T) = apply { Assert.assertEquals(t, this) }
fun <T> T.assertEquals(t: () -> T) = apply { Assert.assertEquals(t(), this) }
infix fun <T> T.assertNotEquals(t: T) = apply { Assert.assertNotEquals(t, this) }
fun <T> T.assertNotEquals(t: () -> T) = apply { Assert.assertNotEquals(t(), this) }
fun <T> T.assertNotNull() = apply { Assert.assertNotNull(this) }
fun <T> T.assertNull() = apply { Assert.assertNull(this) }

fun Boolean.assertTrue() = apply { Assert.assertTrue(this) }
fun Boolean.assertFalse() = apply { Assert.assertFalse(this) }

/**
 * If you use the runBlocking factory you need to have as last expression Unit
 * The following factory avoid that
 */
fun runBlockingUnit(
    context: CoroutineContext = EmptyCoroutineContext,
    block: suspend CoroutineScope.() -> Unit
) = runBlocking { block() }
