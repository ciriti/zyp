package com.itech.datalayer.datasource

import arrow.core.Try
import com.itech.datalayer.net.ApiWrapper
import com.itech.datalayer.util.Location
import com.itech.datalayer.util.Logger

// factory method
fun Datasource.Companion.build(apiWrapper : ApiWrapper, logger : Logger) : Datasource = DatasourceImpl(apiWrapper, logger)

private class DatasourceImpl(
    val apiWrapper : ApiWrapper,
    val logger : Logger
    /** you can use database and other components */) : Datasource{

    override suspend fun getLocations(): Try<List<Location>> {
        // in this place I usually mix logic with db and other components
        return apiWrapper
            .getLocations()
            .map { it.also { logger.info("", it.toString()) } }
            .flatMap { Try{ it } } // for instance
    }

    override suspend fun getInfo(id: Int): Try<Location> {
        return apiWrapper.getInfo(id)
            .map { it.also { logger.info("", it.toString()) } }
            .flatMap { Try{ it } } // for instance
    }
}