package com.itech.samplelocation.presentation.feature.scooterlocation

import androidx.lifecycle.MutableLiveData
import com.itech.samplelocation.core.BaseViewModel
import com.itech.samplelocation.presentation.feature.scooterlocation.BaseState.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

open class ScooterLocationViewModel(
    private val useCase: UseCase,
    dispatcher: CoroutineContext = Dispatchers.Main
) : BaseViewModel(dispatcher){

    val liveData by lazy {
        MutableLiveData<BaseState>()
            .apply { value = StatePositions(emptyList()) }
    }

    fun fetchPositions(){
        launch(job) {
            async(Dispatchers.IO) { useCase.getLocation4View() }.await().fold(
                ::onError,
                ::onSuccess
            )
        }
    }

    fun fetchInfo(id : Int){
        launch(job) {
            async(Dispatchers.IO) { useCase.getInfo4View(id) }.await().fold(
                ::onError,
                ::onSuccess
            )
        }
    }

    fun onSuccess(data : List<Location4View>){
        liveData.value = StatePositions(data)
    }
    fun onSuccess(info : Info4View){
        liveData.value = StateInfo(info)
    }
    fun onError(error : Throwable){
        liveData.value = StateError(getErrorMessage(error))
    }

}