package com.itech.datalayer.datasource

import arrow.core.Try
import com.itech.datalayer.util.Location
import com.itech.datalayer.util.Logger
import com.itech.datalayer.net.ApiWrapper
import com.util.test.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class DatasourceImplTest {

    @MockK
    lateinit var api : ApiWrapper
    @MockK
    lateinit var logger: Logger

    val datasource by lazy { Datasource.build(api, logger) }

    /**
     * files are in root/sharedtestutil/jsonFiles
     */
    val mockList by lazy { TestUtilGson.run { "vehicles.json".createListObjByJsonFile<List<Location>>() } }
    val mockInfo by lazy { TestUtilGson.run { "vehicle_1.json".createGsonObj<Location>() } }

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

    @Test
    fun ` get Locations success case `() = runBlockingUnit{

        coEvery { api.getLocations() } returns Try { mockList }

        val res : Try<List<Location>> = datasource.getLocations()
        (res as Try.Success).assertNotNull()
        res.value.size assertEquals mockList.size
    }

    @Test
    fun ` get Locations error case `() = runBlockingUnit{
        coEvery { api.getLocations() } returns Try { throw RuntimeException() }

        val res : Try<List<Location>> = datasource.getLocations()
        (res as Try.Failure).assertNotNull()
        (res.exception is RuntimeException).assertTrue()
    }

    @Test
    fun ` retrieve info success case `() = runBlockingUnit{

        coEvery { api.getInfo(1) } returns Try { mockInfo }

        val res : Try<Location> = datasource.getInfo(1)
        (res as Try.Success).assertNotNull()
    }

    @Test
    fun ` retrieve info error case `() = runBlockingUnit{
        coEvery { api.getInfo(1) } returns Try { throw RuntimeException() }

        val res : Try<Location> = datasource.getInfo(1)
        (res as Try.Failure).assertNotNull()
        (res.exception is RuntimeException).assertTrue()
    }

}