# Samplelocation
To download the app artifacts, click [HERE](https://gitlab.com/ciriti/samplelocation/-/jobs/255925743/artifacts/download).

![App screenshot](art/app.jpeg "App")

# Overview
## Development approach
- Each component was created following the **SOLID** principles.
  My main goal was to write **scalable**, **testable**, **maintainable**  code.
  
- I've used the  [**Continuous integration**](https://gitlab.com/ciriti/samplelocation/pipelines) 
  practice so that I was able to 
  regularly merge the code changes into a repository. After each commit, 
  builds and tests were **automatically** run. 

## The project structure
These are the modules of the project:  

![Modules of the application](art/modules.png "Modules")

* **datalayer**: contains the datasource Implementation, the network adapter
  and all the abstraction related with the data domain.
  
* **app**: this is the main module containing the application. It gets the 
  references of all the datalayer and it takes care of instantiating
  the implementations of the component and injects them to the view
  components.

## The App Architecture
The app component organization is mainly based on the [google architecture](https://developer.android.com/jetpack/docs/guide),

You can think fo the architecture as a **distinct stack of layers**, each layer:
 * doesn't have knowledge af the upper layer,
 * just provides services to the client layer that uses it,
 * addresses a separate concern.
 
![app architecture](art/architecture.png "App architecture")

## Important libraries used
* **Koin**: in order to avoid creating object inside the view I decide to use koin over 
  the dependency injection lib Dagger 2 because it is faster to setup and to use.
  Koin is an implementation of a service locator pattern and it doesn't check at 
  compile time if the dependencies are satisfied.
* **Retrofit**
* **Arrow**: it is is a collection of different pattern use in the scala world.
* **Android Jetpack architecture**
* I haven't use any lib for the data binding, the android extensions are enough
  in this context.
* **[Mockk](https://github.com/mockk/mockk)**: mocking library for Kotlin.

## Async operation
For async operation I decide to use coroutines.

## Testing
Each single component is **unit tested**. To test the activity 
I used **espresso**.

## Animations
I've used the **Motionlayout** for the cardview animation. 

# Requirements
- Application should open map after start.
- Google Maps SDK should be used.
- Information about vehicles should be retrieved from /vehicles endpoint.
- No authentication is needed.
- Vehicles should be presented as pins on map on retrieved locations.
- On vehicle selection, new card (View) should popup with more detailed information about
- selected vehicles.
- On vehicle deselection, application should hide the card.
- On other vehicle selection, card should display info about newly selected vehicle.
- Retrieving vehicle details should be done using /vehicles/<id> endpoint.

## Notes
Code needs to be well organised, testable and written in Kotlin.
Please create a publicly available Git repository with your solution.
As we are all trying to build bullet proof solutions, unit tests are mandatory.

test
