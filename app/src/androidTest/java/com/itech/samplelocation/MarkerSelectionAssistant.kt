package com.itech.samplelocation

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import com.itech.samplelocation.ViewVerificationHelper.waitAndPerform

class MarkerSelectionAssistant : BaseAssistant() {

    fun selectScooterMarker() : MarkerSelectionAssistant {
        waitAndPerform {
            val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
            val marker = device.findObject(
                UiSelector()
                    .descriptionContains("map")
                    .childSelector(UiSelector().instance(1))
            )
            marker.click()
        }
        return this
    }

    fun clickOnTheMap() : MarkerSelectionAssistant {
        waitAndPerform {
            val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
            val map = device.findObject(
                UiSelector()
                    .descriptionContains("map")
            )
            map.click()
        }
        return this
    }

    fun isViewVisibleOnMap(id: Int)  : MarkerSelectionAssistant{
        isViewVisible(id)
        return this
    }

    fun isViewNotVisibleOnMap(id: Int)  : MarkerSelectionAssistant{
        isViewNotVisible(id)
        return this
    }
}