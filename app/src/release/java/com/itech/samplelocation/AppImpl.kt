package com.itech.samplelocation

import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class AppImpl : App() {

    override fun confVariant() {
        Fabric.with(this, Crashlytics())
    }
}