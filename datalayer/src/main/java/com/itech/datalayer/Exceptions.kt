package com.itech.datalayer

class NoNetworkException @JvmOverloads constructor(
    message: String = "Connection not available",
    cause: Throwable? = null,
    enableSuppression: Boolean = false,
    writableStackTrace: Boolean = true
) : RuntimeException(message, cause, enableSuppression, writableStackTrace)

class GenericException @JvmOverloads constructor(
    message: String? = null,
    cause: Throwable? = null,
    enableSuppression: Boolean = false,
    writableStackTrace: Boolean = true
) : RuntimeException(message, cause, enableSuppression, writableStackTrace)