package com.itech.datalayer.net

import arrow.core.Try
import com.itech.datalayer.util.GenericException
import com.itech.datalayer.util.Location
import com.itech.datalayer.util.Logger

// factory method
fun ApiWrapper.Companion.build(api : Api, logger : Logger) : ApiWrapper = ApiWrapperImpl(api, logger)

private class ApiWrapperImpl(val api : Api, val logger : Logger) : ApiWrapper{

    override suspend fun getLocations(): Try<List<Location>> = Try {
        logger.info("thread type", "${Thread.currentThread().name} ${this.javaClass.simpleName}  ")
        /** here if anything will crash the Try factory will
         * return automatically an Try.Failure obj */
        val response = api.getLocations().await()
        when (response.isSuccessful) {
            true -> response.body()!!
            else -> throw GenericException("Error occurred during fetching tracks")
        }
    }

    override suspend fun getInfo(id: Int): Try<Location> = Try {
        logger.info("thread type", "${Thread.currentThread().name} ${this.javaClass.simpleName}  ")
        /** here if anything will crash the Try factory will
         * return automatically an Try.Failure obj */
        val response = api.getInfo(id).await()
        when (response.isSuccessful) {
            true -> response.body()!!
            else -> throw GenericException("Error occurred during fetching tracks")
        }
    }

}